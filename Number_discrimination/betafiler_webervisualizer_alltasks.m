
clear;clc;
%For this case we are going to analyze three sort of tasks. In test loops
%the file of all betas, t-values and p-values are saved in corresponding
%folder. Afterwards an estimate of the weberfraction is plotted.
load('ratios.mat')

%% number tasks

stname  = ['Number_discrimination\data1\net2\layer2\betas'];

lname   = ['Number_discrimination\data1\net2\layer2\CL_Out_'];

choices = test_loop(lname,stname);

weberestimator(choices,numRatio);

%% convex hull task 

stname = ['convex hull\data2\net2\betas']

lname = ['convex hull\data2\net2\CL_Out_']

choices =test_loop(lname,stname);




%% total surface area task


stname = ['total surface area\data3\net2\betas'];

lname = ['total surface area\data3\net2\CL_Out_'];


choices =test_loop(lname,stname);


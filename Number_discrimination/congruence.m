clear; clc;
dataset = 'NumStim_7to28_100x100';
layer = 2;          % which layer to decode
tr_amount = 1;      % percentage of training set to use
n_runs = 1;         % how many repetitions
guess_rate = 0.01;  % this is fixed
figN = 3;
%net_names = {'NumStimALL-500-1000', 'NumStimALL-500-2000', 'NumStimALL-1000-1500'};
net_names = {'NumStimAll-500-500','NumStimAll-500-1000','NumStimAll-500-1500','NumStimAll-500-2000', 'NumStimAll-1000-500', 'NumStimAll-1000-1000', 'NumStimAll-1000-1500',...
     'NumStimAll-1000-2000','NumStimAll-1500-500', 'NumStimAll-1500-1000','NumStimAll-1500-1500', 'NumStimAll-1500-2000'};
%net_names = {'NumStimALL-1500-1000'}
ages = {'epoch1','epoch10','epoch200'};
for n =[10]
for a = [1,3]
age = ages{a};
if strcmp(age,'epoch1')
    epochs = '1';
elseif strcmp(age, 'epoch10')
    epochs = '10';
else epochs = '200';
end



for s = 1:10
    net_name = [net_names{n} '-ep' epochs '-subj' int2str(s)];   
    load(['data1\net2\layer2\CL_Out_' dataset '-' net_name '-' int2str(round(1*100)) '_layer' int2str(layer)])
    model_choice(:,s) = choice';
end

log_num = round(log2(numRight./numLeft),1)' ;
log_ch = round(log2(TSARight./TSALeft),1)';
log = [log_num, log_ch];

num = round(numRight./numLeft,1)';
 
c =find(log(:,1) < -0.3 & log(:,2) < -0.3 | log(:,1) >= 0.3 & log(:,2) >= 0.3) ;
i =find(log(:,1) > -0.3 & log(:,2) < 0.3 | log(:,1) <= 0.3 & log(:,2) >= -0.3) ;
          


congr = [ num(c,:) ,model_choice(c,:) ];
incongr = [num(i,:) , model_choice(i,:) ];
congruent = grpstats(congr, congr(:,1));
incongruent= grpstats(incongr,incongr(:,1)) ;


 
correctb = logical(correct - ones(size(correct)))';

accuracy = (transpose(correctb) == model_choice);
Acc_congr(a,:,n) = mean(accuracy(c,:));
Acc_incongr(a,:,n) = mean(accuracy(i,:));



end 



matr= [Acc_congr(:,:,n)' ;Acc_incongr(:,:,n)' ];
k = [  zeros(1,10) , ones(1,10) ];

grpstats(matr,k',0.05);
title('congruence of convex hull')
ylabel( 'accuracy')
xticklabels({'congruent', 'incongruent'})


end                                                                                                                                              

% fname = ['data1/net2/congruence1']
% save(fname, 'Acc_congr','Acc_incongr')

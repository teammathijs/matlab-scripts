%accuracy measurement script

 clear; clc;


dataset = 'NumStim_7to28_100x100';
layer = 1;          % which layer to decode
tr_amount = 1;      % percentage of training set to use
n_runs = 1;         % how many repetitions
guess_rate = 0.01;  % this is fixed
figN = 3;
t = 1;

 net_names = { 'NumstimAll-1500-1000'};
 ages = {'epoch1','epoch10','epoch200'};
 
for layer = 1:2
 for a = 1:3
         age = ages{a};
         if strcmp(age,'epoch1')
            epochs = '1';
         elseif strcmp(age, 'epoch10')
            epochs = '10';
         else epochs = '200';
         end

for s = 1:12
net_name = [net_names{1} '-ep' epochs '-subj' int2str(s)];   
load(['data1vs2\net2\CL_Out_' dataset '-' net_name '-' int2str(round(t*100)) '_layer' int2str(layer)]);

max_act =choice + ones(size(choice));

acc = (max_act' == correct);
accuracytotal(a,s,layer) = mean(acc);
ch = round(chRight./chLeft,1)';
num = round(numRight./numLeft,1)';

database = [ num ch];
subsetinc =find(database(:,1) < 1 & database(:,2) > 1 | database(:,1) > 1 & database(:,2) < 1 );
subsetcc = find(database(:,1) < 1 & database(:,2) < 1 | database(:,1) > 1 & database(:,2) > 1);

num = round(numRight./numLeft,1)';

tsa = round(TSARight./TSALeft,1)';
database = [ num tsa];
subsetint =find(database(:,1) < 1 & database(:,2) > 1 | database(:,1) > 1 & database(:,2) < 1 );
subsetct =find(database(:,1) < 1 & database(:,2) <1 | database(:,1) > 1 & database(:,2) > 1  );


congruent_tsa(a,s,layer) = mean(acc(subsetct));
congruent_convexhull(a,s,layer) = mean(acc(subsetcc));
incongruent_tsa(a,s,layer) = mean(acc(subsetint));
incongruent_convexhull(a,s,layer) = mean(acc(subsetinc));


 end
 end
end

%  acctotal = {congruent_tsa,congruent_convexhull,incongruent_tsa,incongruent_convexhull};
% 
%  save(['data1vs2\net1\congruency'], 'acctotal');
% save(['data1vs2\net1\accuracy'],'accuracytotal');

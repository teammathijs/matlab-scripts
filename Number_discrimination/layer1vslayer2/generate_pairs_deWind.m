%% Create numerosity pairs according to specific ratios
%  Then save all pairs into an external file, along with useful
%  information about the stimuli.

clear; clc; %close all;

input  = [];
labels = [];
idxs   = [];
fname = 'NumPairs';
type  = '_TE';
datasets = {'NumStim_7to28_100x100'};
which_dataset = 1;
load(['dataset\' datasets{which_dataset} type]);
load('NumPairsNumStim_7to28_100x100_TE.mat')
       
% small_list = [ 5  6  7  9 10 11 13  6  7  9 10 11 13 15  7 10 11 13 17  5  9 15  6  9 11 13 15 17 19];
% large_list = [10 11 13 17 19 22 25  9 11 13 15 17 19 22  9 13 15 17 22  6 11 19  7 10 13 15 17 19 22];
small_list =   [7  8  9 10  16 18 20 25];
large_list =   [8 10 11 12 20 22 25 28 ];

   n_repetitions = 200; % how many trials for each pair
   show_disp = 0;
   disp_size = sqrt(size(D,1));

  all_idxs_N1 = [];
all_idxs_N2 = [];
for i = 1:length(small_list)
    N1 = small_list(i);
    N2 = large_list(i);
    
    idxs_smaller = find(N_list == N1);
    idxs_larger  = find(N_list == N2);
    
    idxs_N1 = randsample(idxs_smaller,n_repetitions);
    idxs_N2 = randsample(idxs_larger,n_repetitions);
    all_idxs_N1 = [all_idxs_N1 idxs_N1];
    all_idxs_N2 = [all_idxs_N2 idxs_N2];
end

IMGs_N1 = D(:,all_idxs_N1)';
IMGs_N2 = D(:,all_idxs_N2)';

% add also images in the reversed order, and create labels:
input_small_L = [IMGs_N1 IMGs_N2];
labels_small_L = repmat([0 1],size(input_small_L,1),1);
input_large_L = [IMGs_N2 IMGs_N1];
labels_large_L = repmat([1 0],size(input_large_L,1),1);
input = vertcat(input_small_L, input_large_L);
labels = vertcat(labels_small_L, labels_large_L);

idxs_small_L = vertcat(all_idxs_N1, all_idxs_N2);
idxs_small_R = vertcat(all_idxs_N2, all_idxs_N1);
idxs = [idxs_small_L idxs_small_R]';

if show_disp
    for j = 1:20
        selected = randsample(size(input_small_L,1),1);
        figure(11);
        subplot(1,2,1);
        imshow(reshape(input(selected,1:disp_size*disp_size),disp_size,disp_size)); colormap gray;
        subplot(1,2,2);
        imshow(reshape(input(selected,disp_size*disp_size+1:end),disp_size,disp_size)); colormap gray; pause();
    end
end


name = ['dataset\NumPairNumStim_TE3' ];

save(name, 'idxs','input', 'labels');




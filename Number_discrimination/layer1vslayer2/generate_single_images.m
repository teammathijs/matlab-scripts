%% Alberto Testolin
%  alberto.testolin@unipd.it
%  Code to generate numerosity data stimuli as in DeWind et al. (2015)

clear; clc;

fname = 'NumStim_7to28';
type  = '_TE';

disp_x = 200;          % display width
disp_y = 200;          % display height
downsample = 0.5;      % if different than 1, save also scaled images
min_spacing = 4;       % minimum gap between items and from borders
center_displays = 0;   % field area centered on screen?

repetitions = 10;      % how many images for each condition
max_tries = 7000;      % how many trials before trying a different displacement

% new parameters (DeWind had displays of 400x400 pixels)
minNumerosity = 7;
maxNumerosity = 28;
minSize       = 0.26*10^6;  % 0.75*10^6;  [5 to 25: 0.1; 1 to 32: 0.2]
maxSize       = 1.04*10^6;  % 3*10^6;
minSpacing    = 8*10^6;     % 123*10^6;   [5 to 25: 6.5; 1 to 32: 9]
maxSpacing    = 32*10^6;    % 492*10^6;

Num_range = logspace(log10(minNumerosity),log10(maxNumerosity),13);
Siz_range = logspace(log10(minSize),log10(maxSize),13);
Spa_range = logspace(log10(minSpacing),log10(maxSpacing),13);

% test if extreme values fit the display:
% Num_range = [min(Num_range) max(Num_range)];
% Siz_range = [min(Siz_range) max(Siz_range)];
% Spa_range = [min(Spa_range) max(Spa_range)];

tot_patterns = length(Num_range)*length(Siz_range)*length(Spa_range)*repetitions;
D        = zeros(disp_x*disp_y, tot_patterns,'single');
N_list   = zeros(1,tot_patterns,'single');
TSA_list = zeros(1,tot_patterns,'single');
cumArea_list = zeros(1,tot_patterns,'single');
FA_list  = zeros(1,tot_patterns,'single');
CH_list  = zeros(1,tot_patterns,'single');

index = 1; checkDisp = 100000;
[colGrid, rowGrid] = meshgrid(1:disp_x, 1:disp_y);
for i = 1:length(Num_range)
    n = round(Num_range(i));
    for j = 1:length(Siz_range)
        Sz = Siz_range(j);
        for k = 1:length(Spa_range)
            Sp   = Spa_range(k);
            TSA  = sqrt(Sz*n);
            ISA  = sqrt(Sz/n);
            FA   = sqrt(Sp*n);
            
            r = round(sqrt(ISA/pi));   % radius of items
            r_FA = round(sqrt(FA/pi)); % radius of Field Area
            
            % now we can draw the items:
            it = 1;
            while it < repetitions+1
                currDisp = zeros(disp_x, disp_y);
                count = 0; % keep track of the number of items already placed
                tries = 0;
                success = 1;
                
                if center_displays
                    FA_circle = (rowGrid - round(disp_x/2)).^2 + (colGrid - round(disp_y/2)).^2 <= r_FA.^2;
                else
                    FA_center_x = randsample(r_FA:disp_x-r_FA,1);
                    FA_center_y = randsample(r_FA:disp_y-r_FA,1);
                    FA_circle = (rowGrid - FA_center_x).^2 + (colGrid - FA_center_y).^2 <= r_FA.^2;
                end
                
                while count < n
                    if tries > max_tries
                        disp('max tries reached!');
                        success = 0; break
                    end
                    tries = tries + 1;
                    
                    [idx_y, idx_x] = find(FA_circle > 0);
                    where = randsample(length(idx_x),1);
                    pos_x = min(max(idx_x(where),r+min_spacing),disp_x-r-min_spacing);
                    pos_y = min(max(idx_y(where),r+min_spacing),disp_y-r-min_spacing);
                    
                    C = (rowGrid - pos_y).^2 + (colGrid - pos_x).^2 <= r.^2;
                    % check overlap with existing objects before adding it (NB: tolerance included):
                    C1 = (rowGrid - pos_y+min_spacing).^2 + (colGrid - pos_x+min_spacing).^2 <= r.^2;
                    C2 = (rowGrid - pos_y+min_spacing).^2 + (colGrid - pos_x-min_spacing).^2 <= r.^2;
                    C3 = (rowGrid - pos_y-min_spacing).^2 + (colGrid - pos_x+min_spacing).^2 <= r.^2;
                    C4 = (rowGrid - pos_y-min_spacing).^2 + (colGrid - pos_x-min_spacing).^2 <= r.^2;
                    overlap = currDisp .* C;
                    overlap = overlap + (currDisp .* C1) + (currDisp .* C2) + (currDisp .* C3) + (currDisp .* C4);
                    if max(max(overlap)) < 1
                        currDisp = currDisp + C;
                        count = count + 1;
                    end
                end
                
                if success
                    if rem(index,checkDisp)==0
                        figure(1);imshow(currDisp); colormap gray;
                        figure(2);imshow(bwconvhull(currDisp)); colormap gray;
                        figure(3);imshow(FA_circle); colormap gray;
                    end
                    D(:,index) = reshape(currDisp, disp_x*disp_y, 1);
                    N_list(index) = n;
                    TSA_list(index) = TSA;
                    cumArea_list(index) = sum(sum(currDisp));
                    FA_list(index) = FA;
                    CH_list(index) = sum(sum(bwconvhull(currDisp)));
                    index = index + 1;
                    it = it + 1;
                end
            end
        end
    end
end

save(['dataset\' fname '_' int2str(disp_x) 'x' int2str(disp_y) type],'D','N_list','TSA_list','cumArea_list','FA_list','CH_list','-v7.3');

if downsample ~= 1
    D_scaled = zeros(disp_x*disp_y*downsample^2, tot_patterns,'single');
    for p = 1:tot_patterns
        disp = reshape(D(:,p),disp_x,disp_y);
        D_scaled(:,p) = reshape(imresize(disp,downsample),disp_x*disp_y*downsample^2,1);
    end
    D = D_scaled;
    save(['dataset\' fname '_' int2str(disp_x*downsample) 'x' int2str(disp_y*downsample) type],'D','N_list','TSA_list','cumArea_list','FA_list','CH_list');
end

% %% show scatters as in DeWind
% ISA_list = TSA_list./N_list;
% Size = TSA_list.*ISA_list;
% sparsity_FA = FA_list./N_list;
% Spacing = sparsity_FA.*FA_list;
% 
% % NB: real values measured in displays might differ from those specified
% itemSize_list = cumArea_list./N_list;
% Size_real = cumArea_list.*itemSize_list;
% sparsity_CH = CH_list./N_list;
% Spacing_real = sparsity_CH.*CH_list;
% 
% figure(1);
% subplot(2,2,1);
% scatter(ISA_list,TSA_list,10,'filled');
% subplot(2,2,2);
% scatter(log2(ISA_list),log2(TSA_list),10,'filled');
% subplot(2,2,3);
% scatter(sparsity_FA,FA_list,10,'filled');
% subplot(2,2,4);
% scatter(log2(sparsity_FA),log2(FA_list),10,'filled');
% 
% figure(2);
% subplot(2,2,1);
% scatter(itemSize_list,cumArea_list,10,'filled');
% subplot(2,2,2);
% scatter(log2(itemSize_list),log2(cumArea_list),10,'filled');
% subplot(2,2,3);
% scatter(sparsity_CH,CH_list,10,'filled');
% subplot(2,2,4);
% scatter(log2(sparsity_CH),log2(CH_list),10,'filled');

%% display random samples

tot_patterns = size(D,2);
disp_size = sqrt(size(D,1));
r = 10; c = 10;
figure(11);
for j = 1:(r*c)
    n = randsample(tot_patterns,1);
    
    h = subplot(r,c,j);
%     position = get(h, 'pos');
%     position(3) = position(3) + 0.03;
%     position(4) = position(4) + 0.03;
%     set(h, 'pos', position);
    imshow(reshape(D(:,n),disp_size,disp_size));
    axis image; axis off; axis square
end
colormap('gray');



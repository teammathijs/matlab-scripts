clear; clc;
load('dataset\NumStim_7to28_100x100_TE.mat')
%making size and space 

Size = (TSA_list.^2)/N_list;
Space = (FA_list.^2)/length(FA_list);
dataset = [log2(N_list);log2(Size);log2(Space) ]';
[~,~,n] = unique(dataset(:,1));
    combi =[]
%now we are going to reshape our dataset in order to be able to divide our dataset more efficiently later(169)   
for i = 1:13    
       rows= find(n ==i);
       subset =dataset(rows,:);
      [~,~,s]= unique(subset(:,2:3),'rows');
       combi(:,i) = s;
              

    
    
end
combi=combi(:);

for p = 1:max(combi)
    combinations=find(combi == p);
    
    group(:,p) = combinations;
end



%since we need more nodes and combinations of congruent and incongruent, we
%divide the data a little bit more

congruent1 =group(1:60,1:78);
congruent2 = group(71:end , 79: end );
congruent = [ congruent1  congruent2];
congruentsize =congruent(:);  

% (in)congruentspace is the variable for all images in which numerosity and
% space is the same(different)
% from this subset we will select the congruent/incongruent trials in size and space 

incongruent1 = group(71:end, 1:78);
incongruent2 = group(1:60, 79:end);
incongruent = [ incongruent1  incongruent2];
incongruentsize =incongruent(:);

p = 1;
s = 1;

for i = 1:13
    
congruentspace(1:60,s:s+6)  = group(1:60,p:p+6);
incongruentspace(1:60,s:s+6) = group(1:60,p+6:p+12);
congruentspace(61:120,s:s+6)  = group(71:end,p+6:p+12);
incongruentspace(61:120,s:s+6) = group(71:end,p:p+6);
if i < 8
          congruentsispnum(:,s:s+6)  = congruent(:,p:p+6);    
          incongruentnum(:,s:s+6)    = incongruent(:,p:p+6);
          incongruentsi(:,s:s+6)     = incongruent(:,p+6:p+12);
          incongruentspa(:,s:s+6)    = congruent(:,p+6:p+12);
          
elseif (i >=7)
    
           congruentsispnum(:,s:s+6)     = congruent(:,p+6:p+12);
           incongruentnum(:,s:s+6)       = incongruent(:,p+6:p+12);
           incongruentsi(:,s:s+6)        = incongruent(:,p:p+6);
           incongruentspa(:,s:s+6)       = congruent(:,p:p+6); 
end 
       p = 13*i +1;
       s = 7*i + 1;
end
congruentsispnum = congruentsispnum(:, ~any(congruentsispnum == 0));
incongruentnum   = incongruentnum(:, ~any(incongruentnum == 0));
incongruentsi    = incongruentsi(:, ~ any(incongruentsi == 0 ));
incongruentspa   = incongruentspa(:, ~any(incongruentspa == 0));


name = ['dataset/TRIALdataset2.mat'];
save(name, 'congruentspace', 'congruentsize', 'incongruentsize', 'incongruentspace');





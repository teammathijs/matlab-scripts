net_names = {'NumStimAll-500-500','NumStimAll-500-1000','NumStimAll-500-1500','NumStimAll-500-2000', 'NumStimAll-1000-500', 'NumStimAll-1000-1000', 'NumStimAll-1000-1500',...
     'NumStimAll-1000-2000' };
%net_names = {'NumStimAll-500-500','NumStimAll-500-1000'}

layer = 2;
ages = {'epoch1','epoch10','epoch200'};
dataset = 'NumStim_7to28_100x100';
for a = 3
age = ages{a};
if strcmp(age,'epoch1')
    epochs = '1';
elseif strcmp(age,'epoch10')
    epochs = '10'
else
    epochs = '200';
end
for n = 1
for s = 1 
net_name = [net_names{8} '-ep' epochs '-subj' int2str(s)];

 fname_data = ['data1\CL_TE_' dataset '-' net_name  '_layer' int2str(layer)];
 load(fname_data)

 load(['data1\net2\CL_Out_' dataset '-' net_name '-' int2str(round(t*100)) '_layer' int2str(layer)])
 
 
 
 input = input_H(:,1:500);
 
 gem=grpstats( input,numLeft); 
 imagesc(corr(gem'));


gem=grpstats( input,round(chLeft./1000,0)); 
figure()
imagesc(corr(gem'));


gem=grpstats( input,round(TSALeft./1000,1)); 
figure()
imagesc(corr(gem'));

end
end
end

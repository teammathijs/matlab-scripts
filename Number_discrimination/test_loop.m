function[mchoices]  = test_loop(lname,stname)

net_names = {'NumStimAll-500-500','NumStimAll-500-1000','NumStimAll-500-1500','NumStimAll-500-2000', 'NumStimAll-1000-500', 'NumStimAll-1000-1000', 'NumStimAll-1000-1500',...
             'NumStimAll-1000-2000','NumStimAll-1500-500',... 
            'NumStimAll-1500-1000','NumStimAll-1500-1500', 'NumStimAll-1500-2000'};
t = 1;
ages = {'epoch1','epoch10','epoch200'};
layer = 2;        
n_runs = 1;         
guess_rate = 0.01;  
figN = 3;
dataset = 'NumStim_7to28_100x100';

     for a = 1:3
         age = ages{a};
         if strcmp(age,'epoch1')
             epochs = '1';
        elseif strcmp(age, 'epoch10')
            epochs = '10';
        else epochs = '200';
         end
        for n = 1:12
             

           for s = 1:12
            net_name = [net_names{n} '-ep' epochs '-subj' int2str(s)];
            load([lname dataset '-' net_name '-' int2str(round(t*100)) '_layer' int2str(layer)]);
            
            
            model_choice(:,s) = choice(:,t)';
            [model,W]         = num_size_spacing_model(choice',double(numLeft),double(numRight),double(isaLeft),double(isaRight),double(faLeft),double(faRight),double(guess_rate));
            b_num(a,s,n)      = model.Coefficients.Estimate(2);
            b_size(a,s,n)     = model.Coefficients.Estimate(3);
            b_space(a,s,n)    = model.Coefficients.Estimate(4);
            
            pvaluesnum(a,s,n) = model.Coefficients.pValue(2);
            tstatnum(a,s,n)   = model.Coefficients.tStat(2); 
            
            pvaluesize(a,s,n) = model.Coefficients.pValue(3);
            tstatsize(a,s,n)  = model.Coefficients.tStat(3);     
            
            pvaluespace(a,s,n) = model.Coefficients.pValue(4);
            tstatspace(a,s,n) = model.Coefficients.tStat(4);
            
            choices(s,n,:)    = choice;

          
           
        end
    end
mchoices(:,:,a)   =reshape(mean(choices),12,15200);
end
save(stname, 'b_num','b_space','b_size','pvaluesnum','tstatnum','pvaluesize', 'tstatsize', 'pvaluespace','tstatspace');


end


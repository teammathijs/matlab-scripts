clear; clc;


dataset = 'NumStim_7to28_100x100';
layer = 2;          % which layer to decode
tr_amount = 1;      % percentage of training set to use
n_runs = 1;         % how many repetitions
guess_rate = 0.01;  % this is fixed
figN = 3;
%net_names = {'NumStimALL-500-1000', 'NumStimALL-500-2000', 'NumStimALL-1000-1500','NumStimALL-1000-1000','NumStimALL-500-500'};
%net_names = {'NumStimALL-500-500', 'NumStimALL-1000-1000', 'NumStimALL-1000-2000'};
 net_names = {'NumStimAll-500-500','NumStimAll-500-1000','NumStimAll-500-1500','NumStimAll-500-2000', 'NumStimAll-1000-500', 'NumStimAll-1000-1000', 'NumStimAll-1000-1500',...
     'NumStimAll-1000-2000','NumstimAll-1500-500', 'NumstimAll-1500-1000','NumstimAll-1500-1500', 'NumstimAll-1500-2000'};

%net_names = {'NumStimALL-1500-1000'}
ages = {'epoch1','epoch10','epoch200'};
t = 1;
for n = [ 10 ]
    for a = [1 3]
         age = ages{a};
         if strcmp(age,'epoch1')
            epochs = '1';
         elseif strcmp(age, 'epoch10')
            epochs = '10';
         else epochs = '200';
         end



for s = 1:12
      net_name = [net_names{n} '-ep' epochs '-subj' int2str(s)];   
      
      load(['data1\net2\layer2\CL_Out_' dataset '-' net_name '-' int2str(round(t*100)) '_layer' int2str(layer)])
           numRatio = numRight./numLeft;
           matr = [numRatio', choice'];
           gem = grpstats(matr, matr(:,1));
           w = fweber(gem(:,1), gem(:,2) , 'comparison_log');
           fprintf('%.2f\t', w);
           weber(a,s,n) = w;
           accuracy(a,s,n) = te_acc;
           matr = [log2(numRatio)', choice'];
           gem = grpstats(matr, matr(:,1));
           scatter(gem(:,1),gem(:,2));
           


    


end 

end

%  k = categorical({'500-1000 newborn', '500-2000 newborn','1000-1500 newborn';'500-1000mature', '500-2000mature','1000-1500mature'})
% figure(3)
% bar(k,weber);
% ylabel('Weber fraction')
% title('differences in weber fraction in numerosity judgement')
% 
% figure(4);
% bar(k,accuracy);
% ylabel('Accuracy')
% title('differences in accuracy in numerosity judgement')
figure()
subplot(1,2,1);
boxplot(weber(:,:,n)');
title('weber fraction')
ylabel('W')
subplot(1,2,2);
boxplot(accuracy(:,:,n)');
title('accuracy');
ylabel('accuracy');
hold off;
figure();
end

% fname = ['data1/net9/arch1']
% save(fname, 'weber', 'accuracy')
% 


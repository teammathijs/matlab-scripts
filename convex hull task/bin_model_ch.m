
% Calculate other features for left side stimulus
tsaLeft = isaLeft.*numLeft; % calculate total surface area 
sizeLeft = isaLeft.*tsaLeft; % calculate Size
sparLeft = faLeft./numLeft; % calcualte sparsity
spaceLeft = sparLeft.*faLeft; % calculate Spacing

% Calculate other features for right side stimulus
tsaRight = isaRight.*numRight; % calculate total surface area 
sizeRight = isaRight.*tsaRight; % calculate Size
sparRight = faRight./numRight; % calcualte sparsity
spaceRight = sparRight.*faRight; % calculate Spacing

% Calculate right to left ratios
sizeRatio = sizeRight./sizeLeft;
spaceRatio = spaceRight./spaceLeft;
numRatio = numRight./numLeft;
chRatio =  chRight./chLeft;
faRatio = faRight./faLeft;

X = [ log2(chRatio)'  log2(numRatio)' log2(sizeRatio)' log2(spaceRatio)'];

modelx = fitglm(X,choice','Distribution','binomial');

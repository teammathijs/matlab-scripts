


Num_line = [0 0 1 ];
Size_line = [1 0 0];
Spacing_line = [0 1 0];
CH_line = [1/4 0 3/4];
TSA_line = [1/2 0 1/2]; %Remember: the points are in column!
ISA_line = [-1/2 0 1/2];
FA_line = [0 1/2 1/2];
Sparsity_line = [0 -1/2 1/2];
Total_perimeter = [1/4 0 3/4]; %it counts the ratio, not the value
Item_perimeter = [-1/4 0 1/4];
Coverage = [1/2 -1/2 0];
Apparent_closeness = [1/2 1/2 0];
for n = 1:12
for a = 1:3 
    
for s = 1:10
Discr_vect = [ b_size(a,s,n) ,b_space(a,s,n)  , b_num(a,s,n) ];

Vector_Num = (dot(Discr_vect,Num_line)/norm(Num_line)^2)*Num_line;
Vector_Size = (dot(Discr_vect,Size_line)/norm(Size_line)^2)*Size_line;
Vector_Spacing = (dot(Discr_vect,Spacing_line)/norm(Spacing_line)^2)*Spacing_line;
Vector_CH = (dot(Discr_vect, CH_line)/norm(CH_line)^2)*CH_line;
Vector_TSA = (dot(Discr_vect,TSA_line)/norm(TSA_line)^2)*TSA_line;
Vector_ISA = (dot(Discr_vect,ISA_line)/norm(ISA_line)^2)*ISA_line;
Vector_FA = (dot(Discr_vect,FA_line)/norm(FA_line)^2)*FA_line;
Vector_Sparsity = (dot(Discr_vect,Sparsity_line)/norm(Sparsity_line)^2)*Sparsity_line;
Vector_TotPer = (dot(Discr_vect,Total_perimeter)/norm(Total_perimeter)^2)*Total_perimeter;
Vector_ItemPer = (dot(Discr_vect,Item_perimeter)/norm(Item_perimeter)^2)*Item_perimeter;
Vector_Coverage = (dot(Discr_vect,Coverage)/norm(Coverage)^2)*Coverage;
Vector_AppCloseness = (dot(Discr_vect,Apparent_closeness)/norm(Apparent_closeness)^2)*Apparent_closeness;
Origin = [0 0 0];


B_Num = pdist([Vector_Num;Origin]);
B_Size = pdist([Vector_Size;Origin]);
B_Spacing = pdist([Vector_Spacing;Origin]);
B_CH = pdist([Vector_CH;Origin]);
B_TSA = pdist([Vector_TSA;Origin]);
B_ISA = pdist([Vector_ISA;Origin]);
B_FA = pdist([Vector_FA;Origin]);
B_Sparsity = pdist([Vector_Sparsity;Origin]);
B_TotPer = pdist([Vector_TotPer;Origin]);
B_ItemPer = pdist([Vector_ItemPer;Origin]);
B_Coverage= pdist([Vector_Coverage;Origin]);
B_AppCloseness = pdist([Vector_AppCloseness ;Origin]);


Diff_Size = B_Num - B_Size;
Diff_Spacing = B_Num - B_Spacing;
Diff_CH = B_Num - B_CH;
Diff_TSA = B_Num - B_TSA;
Diff_ISA = B_Num - B_ISA;
Diff_FA = B_Num - B_FA;
Diff_Sparsity = B_Num - B_Sparsity;
Diff_Coverage = B_Num - B_Coverage;
Diff_AppCloseness = B_Num - B_AppCloseness;
Diff_TotPer = B_Num - B_TotPer;

differences = [Diff_Size';Diff_Spacing';Diff_CH';Diff_TSA';Diff_ISA';Diff_FA';Diff_Sparsity';Diff_TotPer'];

size_axis = 1;
spacing_axis = 2;
CH_axis = 3;
TSA_axis = 4;
ISA_axis = 5;
FA_axis = 6;
Sparsity_axis = 7;
Coverage_axis = 8;
AppCloseness_axis = 9;
TotPer_axis = 10;
if a == 1
scatter(size_axis,Diff_Size,[], 'b' )
hold on
scatter(spacing_axis,Diff_Spacing,[],'b')
scatter(CH_axis,Diff_CH,[],'b')
scatter(TSA_axis,Diff_TSA,[],'b')
scatter(ISA_axis,Diff_ISA,[],'b')
scatter(FA_axis,Diff_FA,[],'b')
scatter(Sparsity_axis,Diff_Sparsity,[],'b')
scatter(Coverage_axis,Diff_Coverage,[],'b')
scatter(AppCloseness_axis,Diff_AppCloseness,[],'b')
scatter(TotPer_axis,Diff_TotPer,[],'b')
elseif a == 2
    scatter(size_axis,Diff_Size,[], 'r' )
hold on
scatter(spacing_axis,Diff_Spacing,[],'r')
scatter(CH_axis,Diff_CH,[],'r')
scatter(TSA_axis,Diff_TSA,[],'r')
scatter(ISA_axis,Diff_ISA,[],'r')
scatter(FA_axis,Diff_FA,[],'r')
scatter(Sparsity_axis,Diff_Sparsity,[],'r')
scatter(Coverage_axis,Diff_Coverage,[],'r')
scatter(AppCloseness_axis,Diff_AppCloseness,[],'r')
scatter(TotPer_axis,Diff_TotPer,[],'r')
else 
scatter(size_axis,Diff_Size,[], 'g' )
hold on
scatter(spacing_axis,Diff_Spacing,[],'g')
scatter(CH_axis,Diff_CH,[],'g')
scatter(TSA_axis,Diff_TSA,[],'g')
scatter(ISA_axis,Diff_ISA,[],'g')
scatter(FA_axis,Diff_FA,[],'g')
scatter(Sparsity_axis,Diff_Sparsity,[],'g')
scatter(Coverage_axis,Diff_Coverage,[],'g')
scatter(AppCloseness_axis,Diff_AppCloseness,[],'g')
scatter(TotPer_axis,Diff_TotPer,[],'g')
end
end 
xlim([0 10])
ylim([-3 6])
plot(xlim,[0 0],'--','color',[0 0 0] + 0.5)
xticklabels({'','Size','Spacing','convex hull','Total surface area','Item surface area','Field area','Sparsity','Coverage','Apparent closeness','Total Perimeter'})
xtickangle(45)
end
end
title('magnitude of vector projection in convex hull task')
ylabel('b�ta num - magnitude of vector projection');
hold off

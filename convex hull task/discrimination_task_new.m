%% test discrimination ability on pairs of convex hull

clear; clc;
% net_names = {'NumStimALL-500-1000',...
%     'NumStimALL-500-2000',...
%     'NumStimALL-1000-1500'};
net_names = {'NumStimAll-500-500','NumStimAll-500-1000','NumStimAll-500-1500','NumStimAll-500-2000', 'NumStimAll-1000-500', 'NumStimAll-1000-1000', 'NumStimAll-1000-1500',...
     'NumStimAll-1000-2000','NumStimAll-1500-500', 'NumStimAll-1500-1000','NumStimAll-1500-1500', 'NumStimAll-1500-2000'};
 
 %net_names = {'NumStimAll-1500-500', 'NumStimAll-1500-1000','NumStimAll-1500-1500', 'NumStimAll-1500-2000'};

ages = {'epoch1','epoch10','epoch200'};
dataset = 'NumStim_7to28_100x100';

layer = 1; % decoding layer
r = 1;
create_H = 1; % flag to create and save H representations
train_CL = 1; % flag to train the classifier
tr_amount =1; % percentage of available patterns used for training (1 = use all)
n_runs = 1;  % used for collecting statistics

for a = 1:3
age = ages{a};
if strcmp(age,'epoch1')
    epochs = '1';
elseif strcmp(age,'epoch10')
    epochs = '10'
else
    epochs = '200';
end
 for n = [2 6 10]
 for s = 1:12

net_name = [net_names{n} '-ep' epochs '-subj' int2str(s)];

  labelmaker;
%% load files and create TR / TE Hidden activation datasets
if create_H
    type = '_TR'; % TRaining set
    load(['datasets\NumPairs' dataset type]);
    load(['Network2\' age '\' int2str(s) '\' net_name '.mat']);
    [~, disp_size] = size(input);
    disp_size = disp_size / 2;
    H_left  = DBN_activate(DN,input(:,1:disp_size),[]);
    H_right = DBN_activate(DN,input(:,disp_size+1:end),[]);
    H_left = H_left{layer};
    H_right = H_right{layer};
    input_Htr = horzcat(H_left, H_right);
   

    type = '_TE'; % TEst set
    load(['datasets\NumPairs' dataset type]);
    [n_patt, disp_size] = size(input);
    disp_size = disp_size / 2;
    H_left  = DBN_activate(DN,input(:,1:disp_size),[]);
    H_right = DBN_activate(DN,input(:,disp_size+1:end),[]);
    H_left = H_left{layer};
    H_right = H_right{layer};
    input_Hte = horzcat(H_left, H_right);
 

end

%% then train the classifier
if train_CL
    load(['Network2\' age '\' int2str(s) '\' net_name '.mat']);
        tot_tr_patt = size(input_Htr,1);
        tot_patt_to_use = round(tr_amount*tot_tr_patt);
     
        use_idxs_L = randsample(1:tot_tr_patt/2, tot_patt_to_use/2);
        use_idxs_R = randsample(tot_tr_patt/2+1:tot_tr_patt, tot_patt_to_use/2);
        use_idxs = [use_idxs_L use_idxs_R];
        Inp = input_Htr(use_idxs,:);
        Lab = labelsCH(use_idxs,:);
        [weights, ~, ~] = perceptron(Inp, Lab, [], []);
%       [weights, ~, ~] = deltaRule(Inp, Lab, [], []);
         fname = ['data\net2\CL_W_' dataset '-' net_name '-' int2str(round(tr_amount*100)) '_layer' int2str(layer)];
         fname = [fname '_run' int2str(r)];
         save(fname,'weights');
    
end

% test the classifier
Inp = input_Hte;
Lab = labelsCHtest;


    fname_weights = ['data\net2\CL_W_' dataset '-' net_name '-' int2str(round(tr_amount*100)) '_layer' int2str(layer)];
    fname_weights = [fname_weights '_run' int2str(r)];
    load(fname_weights)
    pred = [Inp ones(size(Inp,1),1)] * weights;
    [~, max_act] = max(pred,[],2);
    [correct,~] = find(Lab');
    acc = (max_act == correct);
    te_acc(r) = mean(acc);
    choice(:,r) = logical(max_act - ones(size(max_act)))';


fprintf('Mean accuracy: %.3f\n', mean(te_acc));
fprintf('Std: %.3f\n', std(te_acc));

% prepare data for GLM
load(['datasets\NumPairs' dataset '_TE']);
load(['datasets\' dataset '_TE']);

Num_LeftRight = N_list(idxs);
numLeft = Num_LeftRight(:,1)';
numRight = Num_LeftRight(:,2)';

TSA_LeftRight = TSA_list(idxs); % Total Surface Area
ISA_LeftRight = TSA_LeftRight./Num_LeftRight; % Individual Surface Area
isaLeft = ISA_LeftRight(:,1)';
isaRight = ISA_LeftRight(:,2)';
TSA_real_LeftRight = cumArea_list(idxs);
ISA_real_LeftRight = TSA_real_LeftRight./Num_LeftRight;
isa_real_Left = ISA_real_LeftRight(:,1)';
isa_real_Right = ISA_real_LeftRight(:,2)';
FA_LeftRight = FA_list(idxs); % Field Area
faLeft = FA_LeftRight(:,1)';
faRight = FA_LeftRight(:,2)';
CH_LeftRight = CH_list(idxs); % Convex Hull
chLeft = CH_LeftRight(:,1)';
chRight = CH_LeftRight(:,2)';

fname = ['data\net2\CL_Out_' dataset '-' net_name '-' int2str(round(tr_amount*100)) '_layer' int2str(layer)];
save(fname,'choice','numLeft','numRight','isaLeft','isaRight','isa_real_Left','isa_real_Right','faLeft','faRight','chLeft','chRight','te_acc','correct');

end 
end 
end
% we are going to change the labels now to see whether the model can 
load(['datasets\' dataset '_TR']);
load(['datasets\NumPairs' dataset '_TR']);

first = CH_list(:,idxs(:,1));
second = CH_list(:,idxs(:,2));

labelsCH(:,1) =  (first >= second);
labelsCH(:,2) = (second > first);

load(['datasets\' dataset '_TE']);
load(['datasets\NumPairs' dataset '_TE']);

first = CH_list(:,idxs(:,1));
second = CH_list(:,idxs(:,2));
labelsCHtest(:,1) =  (first >= second);
labelsCHtest(:,2) = (second > first);
    
 
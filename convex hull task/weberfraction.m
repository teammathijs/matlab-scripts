clear; clc;
%net_names = {'NumStimALL-500-1000', 'NumStimALL-500-2000', 'NumStimALL-1000-1500'};
ages = {'epoch1','epoch10','epoch200'};
net_names = {'NumStimAll-500-500','NumStimAll-500-1000','NumStimAll-500-1500','NumStimAll-500-2000', 'NumStimAll-1000-500', 'NumStimAll-1000-1000', 'NumStimAll-1000-1500',...
     'NumStimAll-1000-2000','NumStimAll-1500-500', 'NumStimAll-1500-1000','NumStimAll-1500-1500', 'NumStimAll-1500-2000'};
 dataset = 'NumStim_7to28_100x100';
layer = 2;          % which layer to decode
tr_amount = 1;      % percentage of training set to use
n_runs = 1;         % how many repetitions
guess_rate = 0.01;  % this is fixed
figN = 3;

t = 1;
for n = 1:12
for a = 1:3
age = ages{a};
if strcmp(age,'epoch1')
    epochs = '1';
elseif strcmp(age, 'epoch10')
    epochs = '10';
else epochs = '200';
end




for s = 1:12
      net_name = [net_names{n} '-ep' epochs '-subj' int2str(s)];   
 
    
        load(['data\net2\CL_Out_' dataset '-' net_name '-' int2str(round(t*100)) '_layer' int2str(layer)]);
           chRatio = chRight./chLeft;
               model_choice(:,s) = choice';
               accuracy(a,s,n) = te_acc ;

end 
           matr = [ transpose(round(chRatio,1)), model_choice];
           gem = grpstats(matr, matr(:,1));
           weber(a,:,n) = fweber(gem(:,1), gem(:,2:13) , 'comparison_log');
          
           matr = [ log2(round(chRatio,1)'), model_choice];
           gem = grpstats(matr, matr(:,1));
           figure(a);
           plot(gem(:,1),gem(:,2:10));
 

 



end
hold off;
figure();
subplot(1,2,1);
boxplot(weber(:,:,n)');
title('weber fraction')
ylabel('W')
subplot(1,2,2);
boxplot(accuracy(:,:,n)');
title('accuracy');
ylabel('accuracy');

% k = categorical({'500-1000 newborn', '500-2000 newborn','1000-1500 newborn';'500-1000mature', '500-2000mature','1000-1500mature'});
% figure(3);
% bar(k,weber);
% ylabel('Weber fraction');
% title('differences in weber fraction in convex hull judgement');
% figure(4);
% bar(k,accuracy);
% ylabel('Accuracy');
% title('differences in accuracy in convex hull judgement');

end


fname = ['data/net2/arch']

save(fname, 'weber', 'accuracy')

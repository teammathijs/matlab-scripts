%% What we will do now is make a comparison between the three network groups and different architectures

clear; clc;
% net_names  = {'NumStimALL-500-1000',
%     'NumStimALL-500-2000',
%     'NumStimALL-1000-1500'};
net_names = {'NumStimAll-500-500','NumStimAll-500-1000','NumStimAll-500-1500','NumStimAll-500-2000', 'NumStimAll-1000-500', 'NumStimAll-1000-1000', 'NumStimAll-1000-1500',...
      'NumStimAll-1000-2000','NumStimAll-1500-500', 'NumStimAll-1500-1000','NumStimAll-1500-1500', 'NumStimAll-1500-2000'};
%net_names  = {'NumStimALL-1500-1000'}
t  = 1;      % percentage of training set to use

ages = {'epoch1','epoch10','epoch200'};
layer = 2;          % which layer to decode
n_runs = 1;         % how many repetitions
guess_rate = 0.01;  % this is fixed
figN = 3;
dataset = 'Fer_selected_100x100_TE';
for n = 1:12
    for a = 1:length(ages)
        age = ages{a};
        if strcmp(age,'epoch1')
            epochs = '1';
        elseif strcmp(age, 'epoch10')
            epochs = '10';
        else epochs = '200';
        end
        % print GLM results
        for s = 1:12
            net_name = [net_names{n} '-ep' epochs '-subj' int2str(s)];    
            load(['datafer\svm\CL_Out_' dataset '-' net_name '-' int2str(round(t*100)) '_layer' int2str(layer)])
            X = [N_list,Area_list];
            model = fitglm(X, choice', 'distribution','binomial');
            b_number(a,s,n) = model.Coefficients.Estimate(2);
            b_area(a,s,n) = model.Coefficients.Estimate(3);
            
            
            
        end
    end
    
  epoch1 =[b_area(1,:,:);b_number(1,:,:)];  
  epoch10 =[b_area(2,:,:);b_number(2,:,:)];  
  epoch200 =[b_area(3,:,:);b_number(3,:,:)]; 

    
figure(); 
subplot(2,2,1);
boxplot(epoch1(:,:,n)' );
title('epoch 1')
ylabel('b�ta value')
xticklabels({'b_area', 'b_num'})

subplot(2,2,2);
boxplot(epoch10(:,:,n)');
title('epoch10')
ylabel('b�ta value')
xticklabels({'b_area', 'b_num'})

subplot(2,2,3);
boxplot(epoch200(:,:,n)');
title('epoch200')
ylabel('b�ta value')
xticklabels({'b_area', 'b_num'})



    
    
    
end   

fname = ['datafer/svm/betas']
save(fname, 'b_number','b_area')




%% Simulating the Ferrigno task

clear; clc;

layer = 2; 
ages = {'epoch1','epoch10','epoch200'};
net_names = {'NumStimAll-500-500','NumStimAll-500-1000','NumStimAll-500-1500','NumStimAll-500-2000', 'NumStimAll-1000-500', 'NumStimAll-1000-1000', 'NumStimAll-1000-1500',...
     'NumStimAll-1000-2000', 'NumStimAll-1500-500','NumStimAll-1500-1000','NumStimAll-1500-1500','NumStimAll-1500-2000'};

dataset = 'Fer_selected_100x100_TE';
create_H  = 1; % flag to create and save H representations
train_CL  = 1; % flag to train the classifier
tr_amount = 1; % percentage of available patterns used for training (1 = use all)
n_runs    = 1;  % used for collecting statistics
r         = 1;
t         = 1;

load(['dataset\' dataset ]);

                

for a = [1:3]
   age = ages{a};
   if strcmp(age,'epoch1')
      epochs = '1';
   elseif strcmp(age,'epoch10')
      epochs = '10'
   else
      epochs = '200';
   end
    
   for n = 1:12
        for s = 1:12
                [dataset_TR,labels_TR,idxs] = randomizer(std_catA_stim_net,std_catB_stim_net);
                net_name = [net_names{n} '-ep' epochs '-subj' int2str(s)]

%% load files and create TR / TE Hidden activation datasets
    
    
                load(['Network2\' age '\' int2str(s) '\' net_name '.mat']);
                [~, disp_size] = size(dataset_TR);    
                H = DBN_activate(DN,dataset_TR(:,1:disp_size),[]);
                H_TR = H{layer};
 
                test_trials = probe_stim_net';
                [~, disp_size] = size(test_trials);    
                H = DBN_activate(DN,test_trials(:,1:disp_size),[]);
                H_TE = H{layer};
    
                
  
%% then train the classifier
    
                      
               Inp = double(H_TR); 
               Lab = labels_TR;
               %[weights, ~, ~] = perceptron(Inp, Lab, [], []);
               %[weights, ~, ~] = deltaRule2(Inp, Lab, [], []);
               estimated_model = svmtrain(Lab, Inp, '-s 0 -t 0 -c 100');
               choice = svmpredict(Lab, Inp, estimated_model);


%                pred = [Inp ones(size(Inp,1),1)] * weights;
%                [~,correct]=max(Lab,[],2);
%                correct == max_act;

    
        % test the classifier
               Inp = double(H_TE);
            
               Labels_test = round(rand(5600,1));

               [choice] = svmpredict(Labels_test, Inp, estimated_model);

%                pred = [Inp ones(size(Inp,1),1)] * weights;
               
%                [~, max_act] = max(predicted_label,[],2);
%                choice = logical(max_act - ones(size(max_act)))';


             
       
   
              N_list = probe_table.N_list;
              Area_list = probe_table.Area_list;
              ISA_list = probe_table.ISA_list;
              cumArea_list = probe_table.cumArea_list;
              CH_list = probe_table.CH_list;
              idxs_test = probe_table.original_idx;
       
  
  
    fname = ['datafer\svm\CL_Out_' dataset '-' net_name '-' int2str(round(tr_amount*100)) '_layer' int2str(layer)];
          
    save(fname, 'choice', 'N_list','Area_list','ISA_list','cumArea_list', 'CH_list','idxs_test');           



        end
        
        
    end

end



function [dataset_TR,labels_TR,idxs] = randomizer(data_A,data_B)

X = [randperm(100,100) ,randperm(100,100)+100, randperm(100,100)+200,randperm(100,100) + 300, randperm(100,100)+400]' ; 
 
dataset=  [data_A(:,X)  data_B(:,X)]';
labels = [zeros(size(data_A,2),1) ;ones(size(data_B,2),1)];
idxs=randperm(size(labels,1))';

dataset_TR = dataset(idxs,:);
labels_TR = labels(idxs,:);

end 
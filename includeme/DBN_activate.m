function [DNA] = DBN_activate(DN, INP, LAB)
nPat = size(INP, 1);
actual_layers = size(DN.L, 2); % if DN training is not yet finished (i.e. not all layers have been trained)
for i = 1:actual_layers
    if i == 1
        DNA{1} = single(1./(1 + exp(-single(INP)*DN.L{1}.vishid-repmat(DN.L{1}.hidbiases,nPat,1))));
    else
        if DN.joint && i == DN.nlayers
            DNA{i} = single(1./(1 + exp(-[DNA{i-1} LAB]*DN.L{i}.vishid-repmat(DN.L{i}.hidbiases,nPat,1))));
        else
            DNA{i} = single(1./(1 + exp(-DNA{i-1}*DN.L{i}.vishid-repmat(DN.L{i}.hidbiases,nPat,1))));
        end
    end
end

clear;clc;
load([pwd  '/original task/dataset/Numstim_7to28_100x100_TE.mat']);
D = D(:,1:21900)';
% Problem dimensions
batchsize=100;                                          % !!! HERE DEFINE the batch size !!!
numdims=size(D,2);                                      % Size of vectorized images
totnum =size(D,1);                                      % Total number of images
numbatches=ceil(totnum/batchsize);                      % Calcualte batch size

% Convert images into 3D batch data
batchdata  = zeros(batchsize, numdims, numbatches,'single');  % Empty 3D matrixes for batches.
randomorder=randperm(totnum);                           % (Index of all data reordered
for b=1:numbatches                                      % Distribute 2D vectorized image data to 3D batch data
  Iminibatch=randomorder(1+(b-1)*batchsize:b*batchsize);% Index of one batch
  batchdata(:,:,b) = D(Iminibatch, :);                  % Copy this batch data in the 3D matrix 
end
%


% Save the 3D batchdata in a new file (e.g., StoianovZorzi2012_batchsize100.mat)
save('-v7', sprintf('task_batchsize%d.mat',batchsize), 'batchdata');
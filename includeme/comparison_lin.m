
function y = comparison_lin(w,x)
  y=(1+erf((x-1)./(w*sqrt(x.*x+1))))/2;
end
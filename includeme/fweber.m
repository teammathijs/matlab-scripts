
function [w,r] = fweber(ratios,responses,type)
  [ratios Is]=sort(ratios); 
  [w, r,~,~,~] = nlinfit(ratios,responses(Is),type,.2);
end


function [W] = init_weights(numdims, numhid, std, method)

if strcmp(method,'gaussian')
    W = std*randn(numdims, numhid);
elseif strcmp(method,'gaussianScaled')
    W = randn(numdims, numhid)./sqrt(numdims);
elseif strcmp(method,'sparse')
    n_sparse_connections = 20;
    W = zeros(numdims, numhid);
    for h = 1:numhid
        idxs = randsample(numdims, n_sparse_connections);
        W(idxs,h) = randn(length(idxs), 1);
    end
elseif strcmp(method,'local')
    W = onCenterInit(numdims, numhid);
end

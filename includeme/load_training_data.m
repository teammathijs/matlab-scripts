% ************************************************************************
% Load training patterns and initialize corresponding DBN parameters
%
% Alberto Testolin
% Computational Cognitive Neuroscience Lab
% University of Padova
% ************************************************************************

function [training_set, labels, DN] = load_training_data(DN, dataset)

load([pwd  '/NumStimALL']);

data_images = logical(D');
%data_labels = N_list';

randindexes  = randperm(size(data_images,1));
DN.val_size = 0;
DN.te_size  = 0;
DN.te_idx = [];
training_set = data_images(randindexes,:);
%labels = Labels(randindexes);
labels = [];%data_labels(randindexes,1);
DN.tr_size  = size(data_images,1);
DN.tr_idx = randindexes;

end

function [model, weber] = num_size_spacing_model(choice,numLeft,numRight,isaLeft,isaRight,faLeft,faRight,guessRate)
% This code takes data from a two item forced choice ordinal numerosity
% task (choose greater task) and calculates the Size and Spacing
% parameters. It then calculates the coefficients for the model in equation
% (7) in the attached research article.
%
% Inputs:
% All inputs except the guessRate should be vectors with a value for each
% trial such that choice(15) gives the choice on trial 15.
% 
% choice: a value of 1 means the participant choose stimulus on the right
% side, a 0 means left side.
%
% numLeft: number of items in the left side stimulus.
% 
% numRight: number of items in the right side stimulus.
%
% isaLeft: item surface area of left side stimulus. the surface area of a 
% single item on the left side (all items should be the same size). Units
% are not important so long as the same units are used for all area
% measures.
% 
% isaRight: item surface area of right side stimulus.
% 
% faLeft: field area of the left side stimulus. This should be the area
% of the field within which the dots are drawn.
%
% faRight: field area of right side stimulus.
% 
% guessRate: this is the gamma term from equation (7) in the manuscipt. It
% should be a scalar from 0 - 1 indicating the proportion of trials on 
% which the participant guessed randomly without regard to the stimuli. You
% can fit the model to multiple guessRate values. The model that returns 
% the lowest deviance term is your best estimate of the guess rate.
% 
% Outputs:
% output.beta: a four element vector providing
%  - beta side
%  - beta Number
%  - beta Size
%  - beta Spacing
% in that order.
%
% output.deviance: a statistical term for goodness of fit analagous to the 
% sum of squared error.
% 
% output.stats: a large struct with lots of statistics for hypothesis 
% testing. output.stats.p is the p-values for beta coefficients. See 
% 'glmfit' documentation for more info.
%
% output.w: The w term for numerical acuity.

% Calculate other features for left side stimulus
tsaLeft = isaLeft.*numLeft; % calculate total surface area 
sizeLeft = isaLeft.*tsaLeft; % calculate Size
sparLeft = faLeft./numLeft; % calcualte sparsity
spaceLeft = sparLeft.*faLeft; % calculate Spacing

% Calculate other features for right side stimulus
tsaRight = isaRight.*numRight; % calculate total surface area 
sizeRight = isaRight.*tsaRight; % calculate Size
sparRight = faRight./numRight; % calcualte sparsity
spaceRight = sparRight.*faRight; % calculate Spacing

% Calculate right to left ratios
numRatio = numRight./numLeft;
sizeRatio = sizeRight./sizeLeft;
spaceRatio = spaceRight./spaceLeft;
responseRate = 1 - guessRate; % calculate the 'response rate': the 
% probability that the participant will respond based on the stimulus 
% parameters (not guess blindly)

% Define model

fl = @(mu)norminv((mu-.5)/responseRate+.5); % link
fd = @(mu)1./(responseRate*normpdf(norminv((mu-.5)/responseRate+.5))); % derivative
fi = @(mu)(responseRate*(normcdf(mu)-.5)+.5); % inverse

% Define regressors
X = [log2(numRatio)' log2(sizeRatio)' log2(spaceRatio)' ];

% Fit model
%[output.beta,output.deviance,output.stats] = glmfit(X,choice','binomial','link',{fl fd fi});
model = fitglm(X,choice','Distribution','binomial','link',{fl fd fi});

% calculate w
weber = 1/(sqrt(2)*model.Coefficients.Estimate(2));





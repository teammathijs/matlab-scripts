% dynamically adjust learning rate according to epoch number

function [w_lr vb_lr hb_lr] = set_lr(lr, dyn, epoch, maxepochs, w_lr, vb_lr, hb_lr)
if dyn
%         if rem(epoch-1, floor(maxepochs/10)) == 0
%             w_lr   = gpuArray(lr + (1 - (epoch/maxepochs))/20);
%             vb_lr  = gpuArray(lr + (1 - (epoch/maxepochs))/20);
%             hb_lr  = gpuArray(lr + (1 - (epoch/maxepochs))/20);
%         end
    w_lr = (lr / (1 + 0.01 * epoch));
    vb_lr = w_lr;
    hb_lr = w_lr;
end
end

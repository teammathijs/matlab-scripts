function [mom] = set_mom(mom_init, mom_final, dyn, inc_momentum, mom_epOffset, epoch, maxepochs, mom)

if dyn
    steps = 10;
    epochs_interval = ceil(((maxepochs - mom_epOffset) - inc_momentum)/steps); % max-20
    increase = (mom_final - mom_init)/steps;
    if (epoch > inc_momentum) && rem(epoch-1, epochs_interval) == 0
        mom = gpuArray(mom + increase);
        if mom > 0.85
            mom = 0.85;
        end
    end
%     if (maxepochs-epoch) < 20 && mom > 0.95
%         mom = 0.95;
%     end
else
    if epoch > inc_momentum
        mom = gpuArray(mom_final);
    end
end

% maxepochs = 50;
% mom = 0.4;
% for epoch = 1:maxepochs
%     mom = set_mom(0.4, 0.9, 1, 10, epoch, maxepochs, mom)
% end

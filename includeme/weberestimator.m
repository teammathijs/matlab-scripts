function[] = weberestimator(ratio)
mchoices =reshape(mean(choices),12,15200);
meanchoice=grpstats(mchoices',ratio);
 
 for scat = 1:12 
    scatter(unique(log2(ratio))',meanchoice(:,scat));
    hold on;
 end 
 
[w,r] = fweber(unique(ratio),mean(meanchoice'),'comparison_log')
 
yfit = mean(meanchoice')' - r
plot(unique(log2(ratio))', yfit);
 
 
end
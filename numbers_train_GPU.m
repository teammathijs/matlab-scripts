% ************************************************************************
% Last modified: November 2016
%
% Deep learning on graphic cards to reproduce the numerosity model of
% Stoianov and Zorzi (2012)
%
% Alberto Testolin
% Computational Cognitive Neuroscience Lab
% University of Padova
% ************************************************************************

clear; clc;

datasets = {'S&Z',...
            'S&Z_H1',...
            'S&Z_onCenter_rf6_sig2_H1',...
            'S&Z_OnOffCenter_onlyLocal_H1',...
            'S&Z_OnOffCenter_biases_H1',...
            'NumStimALL'};
dataset  = datasets{6};

how_many_subjects = 12;
net_names = [ 500 1000];
load([pwd  '/datasets/NumStimALL']);
%load([pwd  '/original task/dataset/Numstim_7to28_100x100_TE.mat']);


% DEEP NETWORK SETUP
%layer_sizes   = [500 500; 500 1000; 500 1500; 500 2000; 1000 500; 1000 1000; 1000 1500; 1000 2000];
%layer_sizes   = net_names(n,:);
nlayers       = length(net_names(1,:));
joint_train   = 0;
zipfian       = 0;
CD            = [1];
CD_dyn        = 0;
%maxepochs    = e;
batchsize     = 100;
init_std      = 0.01;
methods       = {'gaussian', 'gaussianScaled', 'sparse', 'local'};
method        = methods{2};
lr_init       = [0.15];
lr_dynamic    = 1;
weight_decay  = [0.0001];
inc_momentum  = 3;
momentum_final= [0.97];
mom_dynamic   = 0;
mom_epOffset  = 0;
momentum_init = 0.7;
sparsity      = 1;
P_range       = [0.2];
shuffle       = 1;

progressive_check = 50;

readout       = 0;
classify_raw_data = 0;
monitoring_steps = 5;
plotRecFields = 0;
recycle       = 0;
plot_idxs_offset = 27;
howmanyneurons = [200 200]; % better if square
generate      = 0;
for n = 1: length(net_names(:,1))
    layer_sizes   = net_names(n,:);
   % randomint= randi([1 60000],1,21970);
    %D = D(:,randomint)';
  for a = [ 200]
      maxepochs     = a;
    for subject = 4:12
        for ls_param = 1:size(layer_sizes,1)
           for mom_param = 1:size(momentum_final,2)
             for lr_param = 1:size(lr_init,2)
                 for wd_param = 1:size(weight_decay,2)
                     for s_param = 1:size(P_range,1)
                        
                        clear batchposhidprobs data data_GPU data_mb new_batchposhidprobs
                        sparse = sparsity;
                        P = P_range(s_param);
                        DN.finalmomentum = momentum_final(mom_param);
                        DN.initialmomentum = momentum_init;
                        DN.mom_dynamic = mom_dynamic;
                        DN.WD         = weight_decay(wd_param);
                        DN.layersize  = layer_sizes(ls_param,:);
                        DN.nlayers    = nlayers;
                        DN.CD         = CD(1);
                        DN.CD_dyn     = CD_dyn;
                        DN.maxepochs  = maxepochs;
                        DN.batchsize  = batchsize(1);
                        DN.lr_init    = lr_init(lr_param);
                        DN.lr_dynamic = lr_dynamic;
                        DN.err        = zeros(DN.maxepochs, DN.nlayers, 'single');
                        DN.sparse     = sparse;
                        DN.joint      = joint_train;
                        DN.fname      = dataset;
                        DN.init       = method;
                        DN.tr_size    = 65910;
                        for i = 1:DN.nlayers
                            DN.fname = [DN.fname '-' int2str(DN.layersize(i))];
                        end
                        if joint_train
                            DN.fname = [DN.fname '-joint'];
                        end
                        if zipfian
                            DN.fname = [DN.fname '-Zipf'];
                        end
                        DN.fname = [DN.fname '-ep' int2str(DN.maxepochs)];
                        if how_many_subjects
                            DN.fname = [DN.fname '-subj' int2str(subject)];
                        end
                        
                       [training_set, labels, DN] = load_training_data(DN, dataset);
                        

                        tot_minibatch = floor(DN.tr_size/DN.batchsize);
                       max_num = max(labels);
                         if zipfian
                         zipf_probs = 1./labels;
                         end
                        
                        start = tic;
                        for layer = 1:DN.nlayers
                            % prepare patterns as a 3D matrix to be loaded in GPU
                            if layer == 1
                                
                                data = zeros(DN.batchsize, size(training_set, 2), tot_minibatch);
                                for i = 1:tot_minibatch
                                   offset = (i-1)*DN.batchsize;
                                   data(:,:,i) = training_set(1+offset: DN.batchsize+offset,:);
                                end
                                clear training_set;
                            else
                                % numhid is previous hidden layer size
                                if (layer == DN.nlayers) && joint_train
                                    oneHot_Labels = zeros(DN.tr_size, max_num);
                                   % for i = 1:DN.tr_size
                                        %oneHot_Labels(i, labels(DN.tr_idx(i))) = 1;
                                        %oneHot_Labels(i, labels(i)) = 1;
                                    %end
                                    data = zeros(DN.batchsize, numhid + max_num, tot_minibatch);
                                    zipf_probs_3D = zeros(DN.batchsize, 1, tot_minibatch);
                                    for i = 1:tot_minibatch
                                        offset = (i-1)*DN.batchsize;
                                        data(:,:,i) = horzcat(batchposhidprobs(1+offset:DN.batchsize+offset,:),...
                                            oneHot_Labels(1+offset:DN.batchsize+offset,:));
                                        zipf_probs_3D(:,:,i) = zipf_probs(1+offset:DN.batchsize+offset);
                                    end
                                else
                                    data = zeros(DN.batchsize, numhid, tot_minibatch);
                                    for i = 1:tot_minibatch
                                        offset = (i-1)*DN.batchsize;
                                        data(:,:,i) = batchposhidprobs(1+offset:DN.batchsize+offset,:);
                                    end
                                end
                            end
                            data_GPU = gpuArray(single(data));
                            % initialize network weights and parameters
                            numhid  = DN.layersize(layer);
                            [numcases, numdims, numbatches] = size(data_GPU);
                            numcases_GPU   = gpuArray(numcases);
                            if strcmp(method,'local') && layer > 1
                                vishid_GPU     = gpuArray(init_weights(numdims, numhid, init_std, methods{3}));
                            else
                                vishid_GPU     = gpuArray(init_weights(numdims, numhid, init_std, method));
                            end
                            hidbiases_GPU  = gpuArray(zeros(1,numhid, 'single'));
                            visbiases_GPU  = gpuArray(zeros(1,numdims, 'single'));
                            vishidinc_GPU  = gpuArray(zeros(numdims, numhid, 'single'));
                            hidbiasinc_GPU = gpuArray(zeros(1,numhid, 'single'));
                            visbiasinc_GPU = gpuArray(zeros(1,numdims, 'single'));
                            new_batchposhidprobs = zeros(DN.tr_size, numhid, 'single');
                            weightcost_GPU = gpuArray(DN.WD);
                            epsilonw_GPU   = gpuArray(DN.lr_init);
                            epsilonvb_GPU  = gpuArray(DN.lr_init);
                            epsilonhb_GPU  = gpuArray(DN.lr_init);
                            momentum_GPU   = gpuArray(momentum_init);
                            
                            for epoch = 1:DN.maxepochs
                               [epsilonw_GPU, epsilonvb_GPU, epsilonhb_GPU] = set_lr(DN.lr_init, DN.lr_dynamic, epoch, DN.maxepochs, epsilonw_GPU, epsilonvb_GPU, epsilonhb_GPU);
                                momentum_GPU = set_mom(DN.initialmomentum, DN.finalmomentum, DN.mom_dynamic, inc_momentum, mom_epOffset, epoch, DN.maxepochs, momentum_GPU);
                                errtot = 0;
                                
                                if shuffle
                                    data_shuff = gather(data_GPU);
                                    clear data_GPU
                                    for r = 1:numcases
                                        idxx = gpuArray(randperm(numbatches));
                                        data_shuff(r,:,:) = data_shuff(r,:,idxx);
                                    end
                                    data_GPU = gpuArray(data_shuff);
                                end
                                
                                for mb = 1:tot_minibatch
                                    
                                    data_mb = data_GPU(:, :, mb);
                                    
                                    if joint_train && zipfian && (layer == DN.nlayers)
                                        zipf_probs_mb = zipf_probs_3D(:,:,mb);
                                        rand_coin = rand(size(zipf_probs_mb));
                                        for i = 1:DN.batchsize
                                            if zipf_probs_mb(i) < rand_coin(i)
                                                data_mb(i, end-max_num+1:end) = zeros(1,max_num);
                                            end
                                        end
                                    end
                                    
                                    rbm_GPU;
                                    errtot = errtot + err_sum;
                                    if epoch == DN.maxepochs
                                        mb_indexes = DN.batchsize*(mb-1)+1:DN.batchsize*(mb-1)+DN.batchsize;
                                        new_batchposhidprobs(mb_indexes, :) = gather(poshidprobs_GPU);
                                    end
                                    if sparse
                                        poshidact = sum(poshidprobs_GPU);
                                        Q = poshidact/DN.batchsize;
                                        if mean(Q) > P
                                            hidbiases_GPU = hidbiases_GPU - epsilonhb_GPU*(Q-P);
                                        end
                                    end
                                end
                                DN.err(epoch, layer) = sqrt(errtot/double(DN.tr_size));
                                
                                % "global" sparseness
                                %                             if sparse
                                %                                 hidbiases_GPU = sparse_coding(sparse, P, tot_minibatch, numcases, DN.tr_size, data_GPU, vishid_GPU, hidbiases_GPU);
                                %                             end
                                
%                                 if rem(epoch, progressive_check) == 0 || epoch == 1
%                                     DN.L{layer}.hidbiases  = gather(hidbiases_GPU);
%                                     DN.L{layer}.vishid     = gather(vishid_GPU);
%                                     DN.L{layer}.visbiases  = gather(visbiases_GPU);
%                                     save (['nn' filesep 'progressive' filesep DN.fname '-' int2str(epoch)], 'DN');
%                                 end
                                
                                % read-out
                                step = floor(DN.maxepochs/monitoring_steps);
                                if rem(epoch, step) == 0
                                    fprintf('Epoch %i \n', epoch);
                                    %                                 fprintf('\t\tlr: %.2f  mom %.2f\n', epsilonw_GPU, momentum_GPU);
                                    %                                 disp(mean(Q));
                                    DN.L{layer}.hidbiases  = gather(hidbiases_GPU);
                                    DN.L{layer}.vishid     = gather(vishid_GPU);
                                    DN.L{layer}.visbiases  = gather(visbiases_GPU);
                                    if readout && ~strcmp(task,'PATCHES')
                                        [DN.CL{layer}, Raw] = decode(task, dataset, DN, classify_raw_data, 1, 1);
                                        fprintf('\tL%i  TR %.2f\n', layer, DN.CL{layer}.trAcc);
                                    end
                                    %figure(1000); plot(DN.err(1:epoch));
                                    %set(gcf, 'PaperPositionMode', 'auto');
                                    %print(gcf,'-dtiff','-painters','-r600', ['rec_error' DN.fname '.tif']);
                                    if plotRecFields
                                        if layer == 1
                                            if recycle
                                                V1 = load('/media/Data/alberto/source code/ORTH/nn/Patch40x40WhitHalfRotN20-1000.mat');
                                                plot_idxs = plot_idxs_offset:size(V1.DN.L{1,1}.vishid,2)+plot_idxs_offset-1;
                                                plot_L1(epoch,DN,howmanyneurons(1), V1.DN, plot_idxs, []);
                                             else
                                                plot_idxs = plot_idxs_offset:size(DN.L{1,1}.vishid,2);
                                                plot_L1(epoch,DN,howmanyneurons(1), [], plot_idxs, []);
                                            end
                                        elseif layer == 2
                                            plot_L2(epoch,DN,howmanyneurons(2), []);
                                        end
                                    end
                                    drawnow; pause(0.1);
                                end
                            end
                            DN.L{layer}.hidbiases  = gather(hidbiases_GPU);
                            DN.L{layer}.vishid     = gather(vishid_GPU);
                            DN.L{layer}.visbiases  = gather(visbiases_GPU);
                            % save also last read-out accuracy in DN structure
                            if readout
                                %[DN.CL{layer} Raw] = decode(task, dataset, DN, classify_raw_data, 1, 1);
                            end
                            batchposhidprobs = new_batchposhidprobs;
                        end
                        
                        DN.learningtime = toc(start);
                        fprintf(1, '\nElapsed time: %d \n', DN.learningtime);
                        name  = ['Network2/','epoch',sprintf('%d', a),'/' ,sprintf('%d', subject)]
                        mkdir(name)
                        cd(name)
                        save(DN.fname,'DN')
                        cd ../../..


                        
                        fprintf('\n');
                        clear DN;
                    end
                end
            end
        end
    end
end
end
end
clear;

% if randomize && (epoch < DN.maxepochs) && rem(epoch, rand_freq) == 0
%     indexes = randperm(DN.tr_size);
%     if layer == 1
%         for i = 1:tot_minibatch
%             offset = (i-1)*DN.batchsize;
%             data(:,:,i) = training_set(indexes(1+offset:DN.batchsize+offset),:);
%         end
%     else
%         data = zeros(DN.batchsize, numdims, tot_minibatch);
%         for i = 1:tot_minibatch
%             offset = (i-1)*DN.batchsize;
%             data(:,:,i) = batchposhidprobs(indexes(1+offset:DN.batchsize+offset),:);
%         end
%     end
%     data_GPU = gpuArray(single(data));
% end





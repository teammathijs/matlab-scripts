%% reconstruction of the image
% in this script, the goal is to reconstruct the image with a random
% network. It is possible to add noise or normalize the biases. Afterwards
% accuracy can be measured to see how well the model is doing. 


load('task_batchsize100.mat', 'batchdata')
load('Network2/epoch200/10/NumStimALL-1500-1000-ep200-subj10.mat')
noise =0 ;

image = batchdata(1,:,20);
state = batchdata(1,:,20);
figure(1)
imagesc(reshape(image,100,100));

%positive phase
for i = [1 2]
  vishid =DN.L{1,i}.vishid;
  if noise == 1
      if i == 1
          vishid = vishid + normrnd(0,0.7,[10000,1500]);
          DN.L{1,1}.vishid = vishid + normrnd(0,1,[10000,1500]);
          %   vishid = normalize(vishid,'scale');
          %   DN.L{1,1}.vishid = normalize(vishid,1);
      end
  end
  hidbiases = DN.L{1,i}.hidbiases;
  state = 1./(1+exp(-state*vishid -hidbiases));

 
end
%negative phase

for i = [2 1]
    visbiases =DN.L{1,i}.visbiases;
    vishid =DN.L{1,i}.vishid;
    statenegative = 1./(1+exp(-state*vishid' - visbiases));
    state = statenegative;
end 
figure(2);
imagesc(reshape(state,100,100));
r =corr(image',state');

%start training data to see if it is performing better or worse 

load(['Number_discrimination\dataset\NumPairsNumStim_7to28_100x100_TR.mat']);
[~, disp_size] = size(input);
disp_size = disp_size / 2;
H_left  = DBN_activate(DN,input(:,1:disp_size),[]);
H_right = DBN_activate(DN,input(:,disp_size+1:end),[]);
H_left = H_left{2};
H_right = H_right{2};
input_H = horzcat(H_left, H_right);
%training data
tot_tr_patt = size(input_H,1);
tot_patt_to_use = round(tot_tr_patt);
    
use_idxs_L = randsample(1:tot_tr_patt/2, tot_patt_to_use/2);
use_idxs_R = randsample(tot_tr_patt/2+1:tot_tr_patt, tot_patt_to_use/2);
use_idxs = [use_idxs_L use_idxs_R];
Inp = input_H(use_idxs,:);
Lab = labels(use_idxs,:);
[weights, ~, ~] = perceptron(Inp, Lab, [], []);

%test data

load(['Number_discrimination\dataset\NumPairsNumStim_7to28_100x100_TE.mat']);
[n_patt, disp_size] = size(input);
disp_size = disp_size / 2;
H_left  =DBN_activate(DN,input(:,1:disp_size),[]);
H_right = DBN_activate(DN,input(:,disp_size+1:end),[]);
H_left  = H_left{2};
H_right = H_right{2};
input_H = horzcat(H_left, H_right);


Inp = input_H;
Lab = labels;
pred = [Inp ones(size(Inp,1),1)] * weights;
[~, max_act] = max(pred,[],2);
[correct,~] = find(Lab');
acc = (max_act == correct);
te_acc = mean(acc)





clear; clc;
net_names = {'NumStimALL-500-1000', 'NumStimALL-500-2000', 'NumStimALL-1000-1500'};
ages = {'newborn','mature'};
ages = {'epoch1','epoch10','epoch200'};
for n =1:3
for a = 1:3
age = ages{a};
if strcmp(age,'epoch1')
    epochs = '1';
elseif strcmp(age, 'epoch10')
    epochs = '10';
else epochs = '200';
end

dataset = 'NumStim_7to28_100x100';
layer = 2;          % which layer to decode
tr_amount = 1;      % percentage of training set to use
n_runs = 1;         % how many repetitions
guess_rate = 0.01;  % this is fixed
figN = 3;

for s = 1:13
    net_name = [net_names{n} '-ep' epochs '-subj' int2str(s)];   
    load(['data3\net2\CL_Out_' dataset '-' net_name '-' int2str(round(1*100)) '_layer' int2str(layer)])
    model_choice(:,s) = choice';

end

log_num = round(log2(numRight./numLeft),1)' ;
log_tsa = round(log2(TSARight./TSALeft),1);
log = [log_tsa , log_num] ;
TSA = round(TSARight./TSALeft,1);

c =find(log(:,1) < -0.2 & log(:,2) < -0.2 | log(:,1) >= 0.2 & log(:,2) >=  0.2) ;
i =find(log(:,1)>  -0.2 & log(:,2) <  0.2 | log(:,1) <= 0.2 & log(:,2) >= -0.2);
          




congr = [ TSA(c,:) ,model_choice(c,:) ];
incongr = [TSA(i,:) , model_choice(i,:) ];
congruent = grpstats(congr, congr(:,1));
incongruent= grpstats(incongr,incongr(:,1)) ;



correctb = logical(correct - ones(size(correct)))';

accuracy = (transpose(correctb) == model_choice);
Acc_congr(:,a,n) = mean(accuracy(c,:));
Acc_incongr(:,a,n) = mean(accuracy(i,:));

hold off;

end
matr= [Acc_congr(:,:,n) ;Acc_incongr(:,:,n) ];
k = [  zeros(1,13) , ones(1,13) ];

figure()
grpstats(matr,k',0.05);
title('congruence')
ylabel( 'accuracy')
xticklabels({'congruent', 'incongruent'})


end

 
fname = ['data3/net2/congruence']
save(fname, 'Acc_congr','Acc_incongr')
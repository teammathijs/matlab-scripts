% we are going to change the labels now to see whether the model can 
load(['datasets\' dataset '_TR']);
load(['datasets\NumPairs' dataset '_TR']);

first = TSA_list(:,idxs(:,1));
second = TSA_list(:,idxs(:,2));

labelsTSA(:,1) =  (first >= second);
labelsTSA(:,2) = (second > first);

load(['datasets\' dataset '_TE']);
load(['datasets\NumPairs' dataset '_TE']);

first = TSA_list(:,idxs(:,1));
second =TSA_list(:,idxs(:,2));
labelsTSAtest(:,1) =  (first >= second);
labelsTSAtest(:,2) =  (second > first);
    
 
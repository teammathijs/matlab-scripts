clear; clc;
net_names = {'NumStimAll-500-500','NumStimAll-500-1000','NumStimAll-500-1500','NumStimAll-500-2000', 'NumStimAll-1000-500', 'NumStimAll-1000-1000', 'NumStimAll-1000-1500',...
      'NumStimAll-1000-2000','NumstimAll-1500-500','NumstimAll-1500-1000','NumstimAll-1500-1500','NumstimAll-1500-2000'};
  ages = {'epoch1','epoch10','epoch200'};
t = 1;
for n= [2 6 10]
for a = 1:3
age = ages{a};
if strcmp(age,'epoch1')
    epochs = '1';
elseif strcmp(age, 'epoch10')
    epochs = '10';
else epochs = '200';
end

dataset = 'NumStim_7to28_100x100';
layer = 1;          % which layer to decode
tr_amount = 1;      % percentage of training set to use
n_runs = 1;         % how many repetitions
guess_rate = 0.01;  % this is fixed
figN = 3;



for s = 1:12
      net_name = [net_names{n} '-ep' epochs '-subj' int2str(s)];   
 
    
        load(['data3\net2\CL_Out_' dataset '-' net_name '-' int2str(round(t*100)) '_layer' int2str(layer)]);
         TSARatio = round(TSARight./TSALeft,1);
                model_choice(:,s) = choice';
               accuracy(a,s,n) = te_acc ;
               
 
           matr = [ round(TSARatio,1), choice];
           gem = grpstats(matr, matr(:,1));
           weber(a,s,n) = fweber(gem(:,1), gem(:,2) , 'comparison_log');
           
           
           matr = [ log2(TSARatio), choice];
           gem = grpstats(matr, matr(:,1));

           figure(a);
           plot(gem(:,1),gem(:,2));
           hold on;

end 
    
hold off;

end

figure();
subplot(1,2,1);
boxplot(weber(:,:,n)');
title('weber fraction')
ylabel('W')
subplot(1,2,2);
boxplot(accuracy(:,:,n)');
title('accuracy');
ylabel('accuracy');


end
fname = ['data3/net2/arch1', ]
save(fname, 'weber', 'accuracy')
%matr=[accuracies' ;accuracy2' ]; matr2= [weberfractions' ; weberfractions2'];

%k = categorical({'newborn','newborn','newborn','newborn','newborn','newborn','newborn','newborn','newborn','newborn','mature','mature','mature','mature','mature','mature','mature','mature','mature','mature'})


%grpstats(matr,k',0.05);
%figure();
%grpstats(matr2, k',0.05);

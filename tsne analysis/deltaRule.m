% ************************************************************************
% Simple perceptron implemented with the iterative delta rule
% 
% NB:
% - learning rate is very sensible
% - multi-class binary classification
%
% Alberto Testolin
% Computational Cognitive Neuroscience Lab
% University of Padova
% ************************************************************************

function [weights, tr_accuracy, te_accuracy] = deltaRule(tr_patterns, tr_labels, te_patterns, te_labels)

te_accuracy = 0;
tr_accuracy = 0;

lr_init = 0.000001;
weight_decay = 0.0001;
max_epochs = 1000;

% add bias
ONES = ones(size(tr_patterns, 1), 1);
tr_patterns = [tr_patterns ONES];

n_input  = size(tr_patterns, 2);
n_output = size(tr_labels, 2);
weights  = 0.1*randn(n_input,n_output); %zeros(n_input,n_output);
final_weights_change = 0;
%pred = tr_patterns * weights;
pred = 1./(1+exp(-tr_patterns*weights));
error = mean(sqrt(sum((tr_labels-pred).^2)/length(tr_labels)));
ep = 0;
while ep <= max_epochs
    ep = ep+1;
    if ep > 100000
        lr = lr_init / (ep/100);% + 0.00001;
    else
        lr = lr_init;
    end
    if ep > 10
        mom = 0.97;
    else
        mom = 0.5;
    end
    
    pred = 1./(1+exp(-tr_patterns*weights));
    %pred = tr_patterns * weights;
    
    weights_change = (tr_labels-pred)'*tr_patterns; % delta rule
    final_weights_change = mom*final_weights_change + lr*(weights_change' - weight_decay*weights);
    weights = weights + final_weights_change;
    
    error = mean(sqrt(sum((tr_labels-pred).^2)/length(tr_labels)));
    errorDelta(ep,:) = error;
    if rem(ep,100)==0;
        figure(1000)
        plot(errorDelta);
        drawnow
    end
end

% test accuracy
pred = tr_patterns*weights;
[~, max_act] = max(pred,[],2);
[r,~] = find(tr_labels'); % find the columns (rows in transpose) are 1
acc = (max_act == r);
tr_accuracy = mean(acc);

if ~isempty(te_patterns)
    te_patterns = [te_patterns ONES];
    pred = te_patterns*weights;
    [~, max_act] = max(pred,[],2);
    [r,~] = find(te_labels');
    acc = (max_act == r);
    te_accuracy = mean(acc);
end

end

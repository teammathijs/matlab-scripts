%% clustering

clear; clc;
%net_names = {'NumStimAll-500-1000','NumStimAll-500-2000','NumStimAll-1000-1500'};
%net_names = {'NumStimAll-500-500','NumStimAll-500-1000','NumStimAll-500-1500','NumStimAll-500-2000', 'NumStimAll-1000-500', 'NumStimAll-1000-1000', 'NumStimAll-1000-1500',...
    % 'NumStimAll-1000-2000', 'NumStimAll-1500-500','NumStimAll-1500-1000','NumStimAll-1500-1500','NumStimAll-1500-2000'};
net_names = {'NumStimAll-500-1000','NumStimAll-1000-1000','NumstimAll-1500-1000'};
sz = 20;
ages = {'epoch1','epoch10','epoch200'};
dataset = 'NumStim_7to28_100x100';
layer     = 2; % decoding layer
tr_amount = 1; % percentage of available patterns used for training (1 = use all)
n_runs    = 1;  % used for collecting statistics
r         = 1;
t         = 1;
load('dataset\NumStim_7to28_100x100_TE.mat');
dividing_images;
D = D';

for a = [1]
    age = ages{a};
    if strcmp(age,'epoch1')
        epochs = '1';
    elseif strcmp(age,'epoch10')
        epochs = '10';
    else
        epochs = '200';
    end
    for n = 3
        for s = 1:12
            net_name = [net_names{n} '-ep' epochs '-subj' int2str(s)]

%% load files and create TR / TE Hidden activation datasets
    
           load(['Network2\' age '\' int2str(s) '\' net_name '.mat']);
           [~, disp_size] = size(D);
           H_1 = DBN_activate(DN,D(:,1:disp_size),[]); 
           H1 = H_1{layer};
           input_H(s,:,:) = horzcat(H1);
%              input_H(s,:,:) = horzcat(D);





        end 
      MeanInput =  reshape(mean(input_H),21970,1000);
      % MeanInput =  reshape(mean(input_H),21970,10000);
    end



[Y1,loss1] = tsne(MeanInput, 'Algorithm', 'barneshut','NumPCAComponents',20);
 scatter(Y1(:,1),Y1(:,2),sz-10,N_list,'filled');
 title(["cluster analysis  all images epoch",epochs,'layer',layer]);
 titel = ['images\cl_analysis_epoch_', epochs,'_',num2str(layer),'.png'];
 titel2 = ['images\cl_analysis_epoch_', epochs,'_',num2str(layer),'.fig'];

 saveas(gcf,titel);
 saveas(gcf,titel2);



 [Y2,loss2] = tsne(MeanInput(congruentsispnum(:),:), 'Algorithm', 'barneshut','NumPCAComponents',20,'perplexity',100);
 scatter(Y2(:,1),Y2(:,2),sz, N_list(congruentsispnum(:)),'filled');
 title(["cluster analysis congruent group epoch",epochs,'layer',layer]);
 titel = ['images\cl_analysis_congr_epoch_', epochs,'_',num2str(layer),'.png'];
 titel2 = ['images\cl_analysis_congr_epoch_', epochs,'_',num2str(layer),'.fig'];
 colorbar;
 saveas(gcf,titel);
 saveas(gcf,titel2);

[Y3,loss3] = tsne(MeanInput(incongruentsize(:),:), 'Algorithm', 'barneshut','NumPCAComponents',20,'perplexity',100);
scatter(Y3(:,1),Y3(:,2),sz, N_list(incongruentsize(:)),'filled');
title(["cluster analysis incongruent size epoch",epochs,'layer',layer]);
titel = ['images\cl_analysis_incongrsize_epoch_', epochs,'_',num2str(layer),'.png'];
titel2 = ['images\cl_analysis_incongrsize_epoch_', epochs,'_',num2str(layer),'.fig'];
saveas(gcf,titel);
saveas(gcf,titel2);

[Y3,loss3] = tsne(MeanInput(incongruentspa(:),:), 'Algorithm', 'barneshut','NumPCAComponents',20,'perplexity',100);
scatter(Y3(:,1),Y3(:,2),sz,N_list(incongruentspa(:)),'filled');
title(["cluster analysis incongruent space epoch",epochs]);
titel = ['images\cl_analysis_incongrspace_epoch_', epochs,'_',num2str(layer),'.png'];
titel2 = ['images\cl_analysis_incongrspace_epoch_', epochs,'_',num2str(layer),'.fig'];

saveas(gcf,titel);
saveas(gcf,titel2);

[Y4,loss4] = tsne(MeanInput(incongruentnum(:),:), 'Algorithm', 'barneshut','NumPCAComponents',20,'perplexity',100);
scatter(Y4(:,1),Y4(:,2), sz,N_list(:,incongruentnum(:)),'filled');
title(["cluster analysis incongruent number epoch",epochs,'layer',layer]);
titel = ['images\cl_analysis_incongrnum_epoch_', epochs,'_',num2str(layer),'.png'];
titel2 = ['images\cl_analysis_incongrnum_epoch_', epochs,'_',num2str(layer),'.fig'];

saveas(gcf,titel);
saveas(gcf,titel2);


% gscatter(Y1(groups,1),Y1(groups,2), N_list(:,groups));
% title(["cluster analysis 4 groups",epochs,'layer' ,layer]);
% titel = ['images\cl_analysis_4groups', epochs,'_',num2str(layer),'.png'];
% saveas(gcf,titel);


end

